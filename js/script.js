document.addEventListener('DOMContentLoaded', function () {
	var sidebarOpener = document.querySelector('#sidebar-opener');

	sidebarOpener.addEventListener('click', function () {
		document.body.classList.toggle('sidebar-showed');
	});
});