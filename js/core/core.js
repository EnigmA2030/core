var Core;
(function (Core) {
    function extend(defaults, options) {
        options = options || {};
        for (var def in defaults) {
            if (typeof options[def] === 'undefined') {
                options[def] = defaults[def];
            }
            else if (typeof options[def] === 'object') {
                for (var deepDef in defaults[def]) {
                    if (typeof options[def][deepDef] === 'undefined') {
                        options[def][deepDef] = defaults[def][deepDef];
                    }
                }
            }
        }
        return options;
    }
    Core.extend = extend;
    function loadImage(src) {
        var img = new Image();
        img.src = src;
        return img;
    }
    Core.loadImage = loadImage;
    function mediaQuery(type, orientation) {
        var queries = {
            tablet: { landscape: [960, 1024], portrait: [768, 959], all: [768, 1024] },
            mobile: { landscape: [480, 767], portrait: [320, 479], all: [320, 767] }
        };
        function getQuery(min, max) {
            return '(min-width: ' + min + 'px) and (max-width: ' + max + 'px)';
        }
        if (type) {
            if (orientation) {
                if (window.matchMedia(getQuery(queries[type][orientation][0], queries[type][orientation][1])).matches) {
                    return true;
                }
            }
            else {
                if (window.matchMedia(getQuery(queries[type]['all'][0], queries[type]['all'][1])).matches) {
                    return true;
                }
            }
        }
        return false;
    }
    Core.mediaQuery = mediaQuery;
    function template(name) {
        var template = document.querySelector(name).textContent;
        return function (data) {
            return doT.template(template)(data);
        };
    }
    Core.template = template;
})(Core || (Core = {}));
