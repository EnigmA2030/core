/// <reference path="../core.ts" />
var Core;
(function (Core) {
    var Slider;
    (function (Slider) {
        var Background = (function () {
            function Background(container, options) {
                this.defaults = {
                    counter: 0,
                    autoplay: false,
                    animation: false,
                    timer: {
                        id: 0,
                        interval: 5000
                    }
                };
                this.container = {};
                this.container.slider = document.querySelectorAll(container);
                if (this.container.slider.length === 0)
                    return;
                this.settings = Core.extend(this.defaults, options);
                this.init();
            }
            Background.prototype.init = function () {
                this.container.slider = this.container.slider[0];
                this.container.slide = this.container.slider.querySelector('.core-slider-slide');
                this.start();
                this.events();
            };
            Background.prototype.events = function () {
                var _this = this;
                this.container.slidePrev = this.container.slider.querySelector('.core-slide-prev');
                this.container.slideNext = this.container.slider.querySelector('.core-slide-next');
                this.container.sliderNumbers = this.container.slider.querySelector('.core-slider-numbers');
                if (this.container.slidePrev) {
                    this.container.slidePrev.addEventListener('click', function () {
                        _this.loadSlidePrev();
                    });
                }
                if (this.container.slideNext) {
                    this.container.slideNext.addEventListener('click', function () {
                        _this.loadSlideNext();
                    });
                }
                if (this.container.sliderNumbers) {
                    this.container.sliderNumbers.addEventListener('click', function (e) {
                        if (e.target.classList.contains('core-slide-number')) {
                            _this.loadSlideNumber(e.target.textContent);
                        }
                    });
                }
            };
            Background.prototype.start = function (resume) {
                var _this = this;
                this.loadSlide();
                if (resume) {
                    this.settings.animation = false;
                }
                if (this.settings.slides.length > 1) {
                    if (this.settings.autoplay) {
                        this.settings.timer.id = setInterval(function () {
                            _this.settings.counter++;
                            if (_this.settings.counter >= _this.settings.slides.length) {
                                _this.settings.counter = 0;
                            }
                            _this.loadSlide();
                        }, this.settings.timer.interval);
                    }
                    if (this.container.slide) {
                        this.container.slide.addEventListener('mouseenter', function () {
                            _this.settings.animation = true;
                            if (_this.settings.timer.id) {
                                clearInterval(_this.settings.timer.id);
                            }
                        });
                        this.container.slide.addEventListener('mouseleave', function () {
                            _this.start(true);
                        });
                    }
                }
            };
            Background.prototype.loadSlide = function () {
                var _this = this;
                if (this.settings.animation) {
                    return false;
                }
                var imageIndex = this.getImageIndex();
                this.container.slider.classList.add('core-slider-loading');
                if (this.container.slide) {
                    this.container.slide.classList.remove('core-slide-loading');
                }
                this.settings.animation = true;
                var slideImage = Core.loadImage(this.settings.slides[this.settings.counter].IMAGES[imageIndex]);
                slideImage.addEventListener('load', function () {
                    _this.container.slider.style.backgroundImage = "url(" + slideImage.src + ")";
                    if (_this.settings.slides[_this.settings.counter].COLOR) {
                        _this.container.slider.style.backgroundColor = _this.settings.slides[_this.settings.counter].COLOR;
                    }
                    if (_this.settings.template) {
                        setTimeout(function () {
                            _this.container.slide.innerHTML = Core.template(_this.settings.template)(_this.settings.slides[_this.settings.counter]);
                            setTimeout(function () {
                                _this.container.slide.classList.add('core-slide-loading');
                            }, 300);
                        }, 300);
                    }
                    _this.container.slider.classList.remove('core-slider-loading');
                    _this.settings.animation = false;
                    _this.changeState();
                });
            };
            Background.prototype.loadSlideNext = function () {
                this.settings.counter++;
                this.loadSlideNumber();
            };
            Background.prototype.loadSlidePrev = function () {
                this.settings.counter--;
                this.loadSlideNumber();
            };
            Background.prototype.loadSlideNumber = function (number) {
                if (number) {
                    this.settings.counter = (number - 1);
                }
                if (this.settings.counter >= this.settings.slides.length) {
                    this.settings.counter = this.settings.slides.length - 1;
                    return false;
                }
                if (this.settings.counter < 0) {
                    this.settings.counter = 0;
                    return false;
                }
                this.loadSlide();
            };
            Background.prototype.getImageIndex = function () {
                var imageIndex = 0;
                if (this.settings.slides[this.settings.counter].IMAGES.length > 1) {
                    if (Core.mediaQuery('tablet')) {
                        imageIndex = (Core.mediaQuery('tablet', 'landscape')) ? 1 : 2;
                    }
                    else if (Core.mediaQuery('mobile')) {
                        imageIndex = (Core.mediaQuery('mobile', 'landscape')) ? 3 : 4;
                    }
                }
                return imageIndex;
            };
            Background.prototype.changeState = function () {
                if (this.container.slidePrev) {
                    this.container.slidePrev.classList.remove('core-slide-prev-disabled');
                    if (this.settings.counter === 0) {
                        this.container.slidePrev.classList.add('core-slide-prev-disabled');
                    }
                }
                if (this.container.slideNext) {
                    this.container.slideNext.classList.remove('core-slide-next-disabled');
                    if (this.settings.counter === this.settings.slides.length - 1) {
                        this.container.slideNext.classList.add('core-slide-next-disabled');
                    }
                }
                if (this.container.sliderNumbers) {
                    for (var _i = 0, _a = this.container.sliderNumbers.children; _i < _a.length; _i++) {
                        var slideNumber = _a[_i];
                        slideNumber.classList.remove('core-slide-number-current');
                    }
                    this.container.sliderNumbers.children[this.settings.counter].classList.add('core-slide-number-current');
                }
            };
            return Background;
        })();
        Slider.Background = Background;
    })(Slider = Core.Slider || (Core.Slider = {}));
})(Core || (Core = {}));
