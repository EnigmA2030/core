/// <reference path="../core.ts" />
namespace Core.Slider {
    export class Background {
        defaults: any = {
            counter: 0,
            autoplay: false,
            animation: false,
            timer: {
                id: 0,
                interval: 5000
            },
        };
        settings: any;
        container: any = {};

        constructor(container: string, options?: any) {
            this.container.slider = document.querySelectorAll(container);

            if (this.container.slider.length === 0) return;

            this.settings = Core.extend(this.defaults, options);

            this.init();
        }

        init() {
            this.container.slider = this.container.slider[0];
            this.container.slide = this.container.slider.querySelector('.core-slider-slide');

            this.start();
            this.events()
        }

        events() {
            this.container.slidePrev = this.container.slider.querySelector('.core-slide-prev');
            this.container.slideNext = this.container.slider.querySelector('.core-slide-next');
            this.container.sliderNumbers = this.container.slider.querySelector('.core-slider-numbers');

            if (this.container.slidePrev) {
                this.container.slidePrev.addEventListener('click', () => {
                    this.loadSlidePrev()
                })
            }

            if (this.container.slideNext) {
                this.container.slideNext.addEventListener('click', () => {
                    this.loadSlideNext()
                })
            }

            if (this.container.sliderNumbers) {
                this.container.sliderNumbers.addEventListener('click', (e) => {
                    if (e.target.classList.contains('core-slide-number')) {
                        this.loadSlideNumber(e.target.textContent)
                    }
                })
            }
        }

        start(resume?: boolean) {
            this.loadSlide();

            if (resume) {
                this.settings.animation = false
            }

            if (this.settings.slides.length > 1) {
                if (this.settings.autoplay) {
                    this.settings.timer.id = setInterval(() => {
                        this.settings.counter++;

                        if (this.settings.counter >= this.settings.slides.length) {
                            this.settings.counter = 0
                        }

                        this.loadSlide();
                    }, this.settings.timer.interval)
                }

                if (this.container.slide) {
                    this.container.slide.addEventListener('mouseenter', () => {
                        this.settings.animation = true;

                        if (this.settings.timer.id) {
                            clearInterval(this.settings.timer.id)
                        }
                    });

                    this.container.slide.addEventListener('mouseleave', () => {
                        this.start(true)
                    })
                }
            }
        }

        loadSlide() {
            if (this.settings.animation) {
                return false
            }

            let imageIndex = this.getImageIndex();

            this.container.slider.classList.add('core-slider-loading');

            if (this.container.slide) {
                this.container.slide.classList.remove('core-slide-loading');
            }

            this.settings.animation = true;

            let slideImage = Core.loadImage(this.settings.slides[this.settings.counter].IMAGES[imageIndex]);

            slideImage.addEventListener('load', () => {
                this.container.slider.style.backgroundImage = "url(" + slideImage.src + ")";

                if (this.settings.slides[this.settings.counter].COLOR) {
                    this.container.slider.style.backgroundColor = this.settings.slides[this.settings.counter].COLOR
                }

                if (this.settings.template) {
                    setTimeout(() => {
                        this.container.slide.innerHTML = Core.template(this.settings.template)(this.settings.slides[this.settings.counter]);
                        setTimeout(() => {
                            this.container.slide.classList.add('core-slide-loading')
                        }, 300)
                    }, 300)
                }

                this.container.slider.classList.remove('core-slider-loading');
                this.settings.animation = false;

                this.changeState();
            });
        }

        loadSlideNext() {
            this.settings.counter++;

            this.loadSlideNumber()
        }

        loadSlidePrev() {
            this.settings.counter--;

            this.loadSlideNumber()
        }

        private loadSlideNumber(number?: number) {
            if (number) {
                this.settings.counter = (number - 1)
            }

            if (this.settings.counter >= this.settings.slides.length) {
                this.settings.counter = this.settings.slides.length - 1;
                return false;
            }

            if (this.settings.counter < 0) {
                this.settings.counter = 0;
                return false;
            }

            this.loadSlide()
        }

        getImageIndex(): number {
            let imageIndex: number = 0;

            if (this.settings.slides[this.settings.counter].IMAGES.length > 1) {
                if (Core.mediaQuery('tablet')) {
                    imageIndex = (Core.mediaQuery('tablet', 'landscape')) ? 1 : 2
                } else if (Core.mediaQuery('mobile')) {
                    imageIndex = (Core.mediaQuery('mobile', 'landscape')) ? 3 : 4
                }
            }

            return imageIndex
        }

        changeState() {
            if (this.container.slidePrev) {
                this.container.slidePrev.classList.remove('core-slide-prev-disabled');

                if (this.settings.counter === 0) {
                    this.container.slidePrev.classList.add('core-slide-prev-disabled')
                }
            }

            if (this.container.slideNext) {
                this.container.slideNext.classList.remove('core-slide-next-disabled');

                if (this.settings.counter === this.settings.slides.length - 1) {
                    this.container.slideNext.classList.add('core-slide-next-disabled')
                }
            }

            if (this.container.sliderNumbers) {
                for (let slideNumber of this.container.sliderNumbers.children) {
                    slideNumber.classList.remove('core-slide-number-current')
                }

                this.container.sliderNumbers.children[this.settings.counter].classList.add('core-slide-number-current');
            }
        }
    }
}