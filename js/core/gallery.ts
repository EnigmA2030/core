/// <reference path="core.ts" />
/**
 * Common namespace
 * @namespace Core
 * @author ITL
 * @copyright ITL
 */
namespace Core {
    /**
     * Create new gallery
     * @class Gallery
     */
    export class Gallery {
        /**
         *  @default
         */
        defaults: any = {
            modal: true,
            container: {
                'detail': '.core-gallery-detail',
                'thumbs': '.core-gallery-thumbs',
            },
            containerModal: {
                'detail': '.core-gallery-modal-detail',
                'thumbs':' .core-gallery-modal-thumbs'
            }
        };

        settings: any;
        json: any = {};

        container: any = {};
        /**
         * Gallery constructor
         * @param {string} container
         * @param {Object} [options]
         */
        constructor(container: string, options?: any) {
            this.container.gallery = document.querySelectorAll(container);

            if (this.container.gallery.length === 0) return;

            this.settings = Core.extend(this.defaults, options);

            this.init();
        }
        /**
         * Initialize gallery
         */
        init() {
            this.container.gallery = this.container.gallery[0];

            this.container.detail = this.container.gallery.querySelector(this.settings.container.detail);
            this.container.thumbs = this.container.gallery.querySelector(this.settings.container.thumbs);

            this.events();
        }
        /**
         * @fires Core.Gallery#detailEvents
         * @fires Core.Gallery#thumbsEvents
         */
        events() {
            if (this.settings.modal && this.settings.template) {
                this.detailEvents()
            }

            if (this.container.thumbs.childElementCount) {
                this.thumbsEvents()
            }
        }
        /**
         * @fires Core.Gallery#detailEvents
         */
        detailEvents() {
            this.container.detail.addEventListener('click', (e) => {
                e.preventDefault();

                let target = Gallery.getTarget(e.target);

                if (target) {
                    this.loadModal(target.href)
                }
            })
        }
        /**
         * @fires Core.Gallery#thumbsEvents
         */
        thumbsEvents() {
            this.container.thumbs.addEventListener('click', (e) => {
                e.preventDefault();

                let target = Gallery.getTarget(e.target);

                if (target) {
                    this.container.detail.firstElementChild.href = target.href;
                    this.loadDetailImage(target.getAttribute('data-img'));
                }
            })
        }
        /**
         * @fires Core.Gallery#thumbsModalEvents
         */
        thumbsModalEvents() {
            this.container.modal.querySelector(this.settings.containerModal.thumbs).addEventListener('click', (e) => {
                e.preventDefault();

                let target = Gallery.getTarget(e.target);

                if (target) {
                    this.loadModalImage(target.href)
                }
            })
        }
        /**
         * @function prepareModal
         */
        prepareModal() {
            this.container.modal = this.container.gallery.querySelector(this.settings.template.container);

            this.json.thumbs = [];

            for (let thumb of this.container.thumbs.children) {
                this.json.thumbs.push({
                    small: thumb.firstElementChild.src,
                    large: thumb.href,
                })
            }
        }
        /**
         * @function loadModal
         * @param {string} src
         */
        loadModal(src: string) {
            this.container.detail.classList.add(this.settings.container.detail + '-loading');

            this.prepareModal();

            let modalImage: HTMLImageElement = Core.loadImage(src);

            modalImage.addEventListener('load', () => {
                this.container.detail.classList.remove(this.settings.container.detail + '-loading');

                this.json.img = modalImage.src;

                this.container.modal.innerHTML = Core.template(this.settings.template.name)(this.json);
                this.container.modal.classList.add('core-gallery-modal-showed');

                this.container.modal.querySelector('.core-gallery-modal-close').addEventListener('click', (e) => {
                    e.preventDefault();

                    this.container.modal.classList.remove('core-gallery-modal-showed');
                });

                this.thumbsModalEvents();
            });
        }
        /**
         * @function loadDetailImage
         * @param {string} src
         */
        loadDetailImage(src: string) {
            this.container.detail.classList.add(this.settings.container.detail + '-loading');

            let detailImage: HTMLImageElement = Core.loadImage(src);

            detailImage.addEventListener('load', () => {
                this.container.detail.firstElementChild.firstElementChild.src = src;
                this.container.detail.classList.remove(this.settings.container.detail + '-loading');
            });
        }
        /**
         * @function loadModalImage
         * @param {string} src
         */
        loadModalImage(src: string) {
            let detail = this.container.modal.querySelector(this.settings.containerModal.detail);

            detail.classList.add(this.settings.containerModal.detail + '-loading');

            let modalImage: HTMLImageElement = Core.loadImage(src);

            modalImage.addEventListener('load', () => {
                detail.classList.remove(this.settings.containerModal.detail + '-loading');
                detail.querySelector('img').src = modalImage.src;
            });
        }
        /**
         * @function getTarget
         * @static
         * @param element
         */
        static getTarget(element) {
            let target;

            if ('A' === element.tagName) {
                target = element
            } else {
                if ('IMG' === element.tagName) {
                    target = element.parentNode
                } else {
                    return false
                }
            }

            return target;
        }
    }
}