declare var doT: any;

namespace Core {
    export function extend(defaults: any, options: any) {
        options = options || {};

        for (let def in defaults) {
            if (typeof options[def] === 'undefined') {
                options[def] = defaults[def];
            }
            else if (typeof options[def] === 'object') {
                for (let deepDef in defaults[def]) {
                    if (typeof options[def][deepDef] === 'undefined') {
                        options[def][deepDef] = defaults[def][deepDef];
                    }
                }
            }
        }

        return options;
    }

    export function loadImage(src: string) {
        let img: HTMLImageElement = new Image();

        img.src = src;

        return img;
    }

    export function mediaQuery(type: string, orientation?: string): boolean {
        let queries: any = {
                tablet: { landscape: [960, 1024], portrait: [768, 959], all: [768, 1024] },
                mobile: { landscape: [480, 767], portrait: [320, 479], all: [320, 767] }
            };

        function getQuery(min: string, max: string): string {
            return '(min-width: ' + min + 'px) and (max-width: ' + max + 'px)'
        }

        if (type) {
            if (orientation) {
                if (window.matchMedia(getQuery(queries[type][orientation][0], queries[type][orientation][1])).matches) {
                    return true
                }
            } else {
                if (window.matchMedia(getQuery(queries[type]['all'][0], queries[type]['all'][1])).matches) {
                    return true
                }
            }
        }

        return false;
    }

    export function template(name: string) {
        let template: string = document.querySelector(name).textContent;

        return function (data: any) {
            return doT.template(template)(data)
        }
    }
}