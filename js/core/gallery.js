/// <reference path="core.ts" />
/**
 * Common namespace
 * @namespace Core
 * @author ITL
 * @copyright ITL
 */
var Core;
(function (Core) {
    /**
     * Create new gallery
     * @class Gallery
     */
    var Gallery = (function () {
        /**
         * Gallery constructor
         * @param {string} container
         * @param {Object} [options]
         */
        function Gallery(container, options) {
            /**
             *  @default
             */
            this.defaults = {
                modal: true,
                container: {
                    'detail': '.core-gallery-detail',
                    'thumbs': '.core-gallery-thumbs'
                },
                containerModal: {
                    'detail': '.core-gallery-modal-detail',
                    'thumbs': ' .core-gallery-modal-thumbs'
                }
            };
            this.json = {};
            this.container = {};
            this.container.gallery = document.querySelectorAll(container);
            if (this.container.gallery.length === 0)
                return;
            this.settings = Core.extend(this.defaults, options);
            this.init();
        }
        /**
         * Initialize gallery
         */
        Gallery.prototype.init = function () {
            this.container.gallery = this.container.gallery[0];
            this.container.detail = this.container.gallery.querySelector(this.settings.container.detail);
            this.container.thumbs = this.container.gallery.querySelector(this.settings.container.thumbs);
            this.events();
        };
        /**
         * @fires Core.Gallery#detailEvents
         * @fires Core.Gallery#thumbsEvents
         */
        Gallery.prototype.events = function () {
            if (this.settings.modal && this.settings.template) {
                this.detailEvents();
            }
            if (this.container.thumbs.childElementCount) {
                this.thumbsEvents();
            }
        };
        /**
         * @fires Core.Gallery#detailEvents
         */
        Gallery.prototype.detailEvents = function () {
            var _this = this;
            this.container.detail.addEventListener('click', function (e) {
                e.preventDefault();
                var target = Gallery.getTarget(e.target);
                if (target) {
                    _this.loadModal(target.href);
                }
            });
        };
        /**
         * @fires Core.Gallery#thumbsEvents
         */
        Gallery.prototype.thumbsEvents = function () {
            var _this = this;
            this.container.thumbs.addEventListener('click', function (e) {
                e.preventDefault();
                var target = Gallery.getTarget(e.target);
                if (target) {
                    _this.container.detail.firstElementChild.href = target.href;
                    _this.loadDetailImage(target.getAttribute('data-img'));
                }
            });
        };
        /**
         * @fires Core.Gallery#thumbsModalEvents
         */
        Gallery.prototype.thumbsModalEvents = function () {
            var _this = this;
            this.container.modal.querySelector(this.settings.containerModal.thumbs).addEventListener('click', function (e) {
                e.preventDefault();
                var target = Gallery.getTarget(e.target);
                if (target) {
                    _this.loadModalImage(target.href);
                }
            });
        };
        /**
         * @function prepareModal
         */
        Gallery.prototype.prepareModal = function () {
            this.container.modal = this.container.gallery.querySelector(this.settings.template.container);
            this.json.thumbs = [];
            for (var _i = 0, _a = this.container.thumbs.children; _i < _a.length; _i++) {
                var thumb = _a[_i];
                this.json.thumbs.push({
                    small: thumb.firstElementChild.src,
                    large: thumb.href
                });
            }
        };
        /**
         * @function loadModal
         * @param {string} src
         */
        Gallery.prototype.loadModal = function (src) {
            var _this = this;
            this.container.detail.classList.add(this.settings.container.detail + '-loading');
            this.prepareModal();
            var modalImage = Core.loadImage(src);
            modalImage.addEventListener('load', function () {
                _this.container.detail.classList.remove(_this.settings.container.detail + '-loading');
                _this.json.img = modalImage.src;
                _this.container.modal.innerHTML = Core.template(_this.settings.template.name)(_this.json);
                _this.container.modal.classList.add('core-gallery-modal-showed');
                _this.container.modal.querySelector('.core-gallery-modal-close').addEventListener('click', function (e) {
                    e.preventDefault();
                    _this.container.modal.classList.remove('core-gallery-modal-showed');
                });
                _this.thumbsModalEvents();
            });
        };
        /**
         * @function loadDetailImage
         * @param {string} src
         */
        Gallery.prototype.loadDetailImage = function (src) {
            var _this = this;
            this.container.detail.classList.add(this.settings.container.detail + '-loading');
            var detailImage = Core.loadImage(src);
            detailImage.addEventListener('load', function () {
                _this.container.detail.firstElementChild.firstElementChild.src = src;
                _this.container.detail.classList.remove(_this.settings.container.detail + '-loading');
            });
        };
        /**
         * @function loadModalImage
         * @param {string} src
         */
        Gallery.prototype.loadModalImage = function (src) {
            var _this = this;
            var detail = this.container.modal.querySelector(this.settings.containerModal.detail);
            detail.classList.add(this.settings.containerModal.detail + '-loading');
            var modalImage = Core.loadImage(src);
            modalImage.addEventListener('load', function () {
                detail.classList.remove(_this.settings.containerModal.detail + '-loading');
                detail.querySelector('img').src = modalImage.src;
            });
        };
        /**
         * @function getTarget
         * @static
         * @param element
         */
        Gallery.getTarget = function (element) {
            var target;
            if ('A' === element.tagName) {
                target = element;
            }
            else {
                if ('IMG' === element.tagName) {
                    target = element.parentNode;
                }
                else {
                    return false;
                }
            }
            return target;
        };
        return Gallery;
    })();
    Core.Gallery = Gallery;
})(Core || (Core = {}));
